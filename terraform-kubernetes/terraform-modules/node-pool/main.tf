resource "google_container_node_pool" "pool-1" {
  provider = "google-beta"
  name       = "pool-1"
  location   = var.zone
  cluster    = var.cluster_name
  node_count = var.node_count
  project    = var.project_id

  autoscaling {
    max_node_count = var.autoscaling_max_node_count
    min_node_count = var.autoscaling_min_node_count
  }

  node_config {
    machine_type = var.machine_type_hub
    disk_size_gb = var.disk_size_gb
    disk_type = "pd-ssd"
    labels = {
      node_type  = "hub"
    }

  }
}
