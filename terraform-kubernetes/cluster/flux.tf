resource "kubernetes_namespace" "infrastruture-namespace" {
  depends_on = [null_resource.configure_kubectl]
  metadata {
    name = "infra"
  }
}

resource "kubernetes_secret" "flux_ssh" {
  depends_on = [kubernetes_namespace.infrastruture-namespace]

  metadata {
    name = var.git_secretName
    namespace = kubernetes_namespace.infrastruture-namespace.id
  }
  data = {
    "identity" = "${file(var.flux_key_path)}"
  }
}

resource "helm_release" "fluxcd" {
  depends_on = [kubernetes_secret.flux_ssh]
  name  = "fluxcd"
  repository = "https://charts.fluxcd.io"
  chart = "flux"
  namespace = kubernetes_namespace.infrastruture-namespace.id

  set {
    name  = "git.url"
    value = var.git_url
  }

  set {
    name  = "git.secretName"
    value = var.git_secretName
  }

  set {
    name = "git.branch"
    value = var.git_branch
  }

  set {
    name = "git.label"
    value = var.git_label
  }

  set {
    name = "clusterRole.create"
    value = var.flux_cluster_role
  }

  set {
    name = "syncGarbageCollection.enabled"
    value = "true"
  }
}

