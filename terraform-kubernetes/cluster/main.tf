# create network to run cluster instances
module "my_network" {
  source                    = "../terraform-modules/vpc"
  name                      = var.network_name
  project_id                = var.project_id
  nodes_network             = var.nodes_network
  subnetwork                = var.subnetwork
}

# create cluster
module "my_cluster" {
  source                    = "../terraform-modules/cluster"
  name                      = var.name
  project                   = var.project_id
  description               = var.description
  zone                      = var.zone
  initial_node_count        = var.initial_node_count
  network                   = var.network_name
  nodes_network             = var.nodes_network
  pods_network              = var.pods_network
  subnetwork                = var.subnetwork
  services_network          = var.services_network
}

# create gitlab node pool and attach it to my-cluster
module "node-pools" {
  source                    = "../terraform-modules/node-pool"
  name                      = var.node_pool_name
  zone                      = var.zone
  cluster_name              = module.my_cluster.name
  node_count                = 2
  project_id                = var.project_id
  machine_type_hub          = var.machine_type_hub
  machine_type_browser      = var.machine_type_browser
  autoscaling_max_node_count  = var.autoscaling_max_node_count
  autoscaling_min_node_count  = var.autoscaling_min_node_count

}


data "google_client_config" "current" {}

# configure kubectl with the credentials of the GKE cluster
resource "null_resource" "configure_kubectl" {
  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${module.my_cluster.name} --zone ${var.zone} --project ${var.project_id}"
//    environment = {
//      KUBECONFIG = var.kubectl_config_path != "" ? var.kubectl_config_path : ""
//    }
  }

  depends_on = [module.node-pools]
}

