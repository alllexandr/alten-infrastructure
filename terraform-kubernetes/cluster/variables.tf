## ---------------------
## Provider configuration
## ---------------------
variable "project_id" {
  description = "Project ID in GCP"
}

variable "region" {
  description = "Region in which to manage GCP resources"
}

## ---------------------
## Cluster configuration
## ---------------------
variable "name" {
  description = "The name of the cluster, unique within the project and zone"
}

variable "network_name" {
  description = "The name of the network to create to run cluster instances"
}

variable "description" {
  description = "Cluster description"
}

variable "zone" {
  description = "The zone the master and nodes specified in initial_node_count should be created in"
}

variable "initial_node_count" {
  description = "Number of nodes in the cluster"
  default     = 1
}

variable "machine_type_hub" {
  description = "The type of machine to use for hub pool"
  default     = "n1-standard-4"
}

variable "machine_type_browser" {
  description = "The type of machine to use for browser pool"
  default     = "n1-standard-1"
}

# network
variable "subnetwork" {
  description = "The name of the Google Compute Engine network subnetwork"
  default     = "cluster-subnetwork"
}

variable "nodes_network" {
  description = "Custom ip nodes range"
  default = "10.50.25.0/24"
}

variable "pods_network" {
  description = "Pods network"
  default = "10.50.128.0/18"
}

variable "services_network" {
  description = "Services network"
  default = "10.50.192.0/18"

}

variable "default_max_pods_per_node" {
  description = "Max pods per node"
  default = 32
}

variable "node_pool_name" {
  description = "Node pool name"
}

variable "autoscaling_max_node_count" {
  description = "Max node count in the pool"
  default = 5
}

variable "autoscaling_min_node_count" {
  description = "Min node count in the pool"
  default = 1
}


// Variables for FluxCD
variable "git_url" {}
variable "git_secretName" {default = "flux-ssh" }
variable "git_branch" {default = "master" }
variable "git_label" {default = "flux_sync" }
variable "flux_cluster_role" { default = true }
variable "flux_key_path" { default = "./flux-key"}

