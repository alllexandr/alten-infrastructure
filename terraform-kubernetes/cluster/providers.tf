provider "google-beta" {
  version = "~> 2.5"
  project = var.project_id
  region  = var.region
}
