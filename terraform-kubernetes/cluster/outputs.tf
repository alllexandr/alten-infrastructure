
output "kubconfig" {
  value = format("... \nRun command to configure access via kubectl:\n$ gcloud container clusters get-credentials %s --zone %s --project %s",module.my_cluster.name, var.zone, var.project_id )
}

